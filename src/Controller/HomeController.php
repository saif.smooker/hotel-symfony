<?php

namespace App\Controller;

use App\Entity\Chambre;
use App\Repository\ChambreRepository;
use App\Entity\Client;
use App\Repository\ClientRepository;
use App\Entity\Reservation;
use App\Repository\ReservationRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/")
 */
class HomeController extends AbstractController
{
    /**
     * @Route("/", name="dashboard", methods={"GET"})
     */
    public function index(ChambreRepository $chambreRepository,ClientRepository $clientRepository, ReservationRepository $reservationRepository): Response
    {
        return $this->render('index.html.twig', [
            'chambres' => $chambreRepository->findAll(),
            'clients' => $clientRepository->findAll(),
            'reservations' => $reservationRepository->findAll(),
        ]);
    }

}
