<?php

namespace App\Entity;

use App\Repository\ReservationRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ReservationRepository::class)
 */
class Reservation
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Client::class, inversedBy="reservations")
     * @ORM\JoinColumn(nullable=false)
     */
    private $id_client;

    /**
     * @ORM\ManyToOne(targetEntity=Chambre::class, inversedBy="reservations")
     * @ORM\JoinColumn(nullable=false)
     */
    private $id_chambre;

    /**
     * @ORM\Column(type="string")
     */
    private $date_arr;

    /**
     * @ORM\Column(type="string")
     */
    private $date_dep;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdClient(): ?Client
    {
        return $this->id_client;
    }

    public function setIdClient(?Client $id_client): self
    {
        $this->id_client = $id_client;

        return $this;
    }

    public function getIdChambre(): ?Chambre
    {
        return $this->id_chambre;
    }

    public function setIdChambre(?Chambre $id_chambre): self
    {
        $this->id_chambre = $id_chambre;

        return $this;
    }

    public function getDateArr()
    {
        return $this->date_arr;
    }

    public function setDateArr( $date_arr): self
    {
        $this->date_arr = $date_arr;

        return $this;
    }

    public function getDateDep()
    {
        return $this->date_dep;
    }

    public function setDateDep($date_dep): self
    {
        $this->date_dep = $date_dep;

        return $this;
    }
}
